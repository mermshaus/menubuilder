<?php

use mermshaus\NavGenerator\Generator;

error_reporting(-1);
ini_set('display_errors', 1);

require_once __DIR__ . '/../../vendor/autoload.php';

$menuData = <<<EOT
# Lines starting with a hash sign (#) will be ignored
#
# Whitespace before/after the separator doesn't matter.
# Indentation with exactly 4 spaces per level is important.
Home                  ; /

About                 ; /about
    The Company       ; /about/company
        Germany       ; /germany
        Europe        ; /europe
        ROW           ; /about/company/row
    Team              ; /about/team
    Careers           ; /about/careers

Services              ; /services
    What we do        ; /services/what-we-do
    Our process       ; /services/our-process
    Testimonials      ; /services/testimonials

Portfolio                    ; /portfolio
    Web Design               ; /portfolio/webdesign
    Development              ; /portfolio/development
    Identity                 ; /portfolio/identity
    SEO & Internet Marketing ; /portfolio/seo-marketing
    Print Design             ; /portfolio/print-design

Contact               ; /contact
EOT;

$navGenerator = new Generator();

$url = 'data://text/plain;base64,' . base64_encode($menuData);
$stream = fopen($url, 'rb');
$navGenerator->readFlatFile($stream);
fclose($stream);

$menuHtml = $navGenerator->renderMenu();

$rootUrl = rtrim(dirname($_SERVER['SCRIPT_NAME']), '/');

header('Content-Type: text/html; charset=UTF-8');

?><!DOCTYPE html>

<html lang="en">

    <head>
        <meta charset="UTF-8" />
        <title>title</title>
        <link href="<?=$rootUrl?>/line25.css" media="screen" rel="stylesheet" type="text/css">
    </head>

    <body>
        <nav>
        <?=$menuHtml?>
        </nav>

        <p style="text-align: center;">
            Demo CSS taken from
            <a href="http://line25.com/tutorials/how-to-create-a-pure-css-dropdown-menu">line25.com</a>
        </p>
    </body>

</html>
