function Storage()
{
    this.dataContainer = {};
    this.read();
}

/* --------- Private Methods --------- */

Storage.prototype.linearize = function()
{
    var string = "", name, value;
    for (name in this.dataContainer)
    {
        name = encodeURIComponent(name);
        value = encodeURIComponent(this.dataContainer[name]);
        string += name + "=" + value + "&";
    }
    if (string != "")
    {
        string = string.substring(0, string.length - 1);
    }
    return string;
}

Storage.prototype.read = function()
{
    if (window.name == '' || window.name.indexOf("=") == -1) {
        return;
    }
    var pairs = window.name.split("&");
    var pair, name, value;
    for (var i = 0; i < pairs.length; i++) {
        if (pairs[i] == "") {
            continue;
        }
        pair = pairs[i].split("=");
        name = decodeURIComponent(pair[0]);
        value = decodeURIComponent(pair[1]);
        this.dataContainer[name] = value;
    }
}

Storage.prototype.write = function()
{
    window.name = this.linearize();
}

/* --------- Public Methods --------- */

Storage.prototype.set = function(name, value)
{
    this.dataContainer[name] = value;
    this.write();
}

Storage.prototype.get = function(name)
{
    var returnValue = this.dataContainer[name];
    return returnValue;
}

Storage.prototype.getAll = function()
{
    return this.dataContainer;
}

Storage.prototype.remove = function(name)
{
    if (typeof(this.dataContainer[name]) != undefined)
    {
        delete this.dataContainer[name];
    }
    this.write();
}

Storage.prototype.removeAll = function()
{
    this.dataContainer = {};
    this.write();
}
