<?php

#namespace org\ermshaus\menubuilder;

/**
 * Enter description here...
 *
 */
class org_ermshaus_menubuilder_MenuEntry
{
    /** */
    protected $m_children;

    /** */
    protected $m_text;

    /** */
    protected $m_url;

    /**
     * Enter description here...
     *
     * @param string $text
     * @param string $url
     */
    public function __construct($text, $url)
    {
        $this->m_children = array();
        $this->m_text = $text;
        $this->m_url = $url;
    }

    /**
     * Enter description here...
     *
     * @param org_ermshaus_menubuilder_MenuEntry $me
     */
    public function addChild(org_ermshaus_menubuilder_MenuEntry $me)
    {
        $this->m_children[] = $me;
    }

    /**
     * Enter description here...
     *
     * @return int
     */
    public function countChildren()
    {
        return count($this->m_children);
    }

    /**
     * Enter description here...
     *
     * @return array
     */
    public function getChildren()
    {
        return $this->m_children;
    }

    /**
     * Enter description here...
     *
     * @return string
     */
    public function getText()
    {
        return $this->m_text;
    }

    /**
     * Enter description here...
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->m_url;
    }

    /**
     * Enter description here...
     *
     * @param string $text
     */
    public function setText($text)
    {
        $this->m_text = $text;
    }

    /**
     * Enter description here...
     *
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->m_url = $url;
    }
}
