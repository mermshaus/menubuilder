# menubuilder

There is no stable release version available yet.

* **v0.1.0** - Menu version with JavaScript functionality and storage of
  expanded paths via `window.name`. In commits after v0.1.0, this version is
  still available in `./demos/old-js-version`. Don’t expect it to work very
  well.


## Goals

* Goal 1…
* Goal 2…


## Install

Via Composer **(not yet available)**

``` bash
$ composer require mermshaus/menubuilder
```


## Requirements

* PHP >= 5.3

(Tested with PHP 5.5.)


## Documentation

``` php
$generator = new Generator();
```


## Testing

``` bash
$ phpunit
```


## License

The MIT License (MIT). Please see the `LICENSE` file for more information.
