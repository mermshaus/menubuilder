<?php

namespace mermshaus\NavGenerator;

use Closure;

/**
 *
 */
final class Config
{
    /**
     *
     * @var string
     */
    private $pathPrefix;

    /**
     *
     * @var string
     */
    private $activePath;

    /**
     *
     * @var string
     */
    private $cssClassActive;

    /**
     *
     * @var string
     */
    private $cssClassOnPath;

    /**
     *
     * @var Closure
     */
    private $escapeFunction;

    /**
     *
     * @param string $pathPrefix
     * @param string $activePath
     * @param string $cssClassActive
     * @param string $cssClassOnPath
     * @param Closure $escapeFunction
     */
    public function __construct(
        $pathPrefix = null,
        $activePath = null,
        $cssClassActive = 'selected',
        $cssClassOnPath = 'onpath',
        Closure $escapeFunction = null
    ) {
        \Assert\that($pathPrefix)->nullOr()->string();
        \Assert\that($activePath)->nullOr()->string();
        \Assert\that($cssClassActive)->string();
        \Assert\that($cssClassOnPath)->string();

        $requestUri = (isset($_SERVER['REQUEST_URI']))
                ? $_SERVER['REQUEST_URI']
                : '';

        // We'll use our best guess as default
        $this->pathPrefix = ($pathPrefix !== null)
                ? $pathPrefix
                : rtrim(dirname($_SERVER['SCRIPT_NAME']), '/');

        $this->activePath = ($activePath !== null)
                ? $activePath
                : trim(substr($requestUri, strlen($this->pathPrefix)));

        $this->cssClassActive = $cssClassActive;
        $this->cssClassOnPath = $cssClassOnPath;

        $this->escapeFunction = ($escapeFunction !== null)
                ? $escapeFunction
                : function ($s) {
                    return htmlspecialchars($s, ENT_QUOTES, 'UTF-8');
                };
    }

    /**
     *
     * @return string
     */
    public function getPathPrefix()
    {
        return $this->pathPrefix;
    }

    /**
     *
     * @return string
     */
    public function getActivePath()
    {
        return $this->activePath;
    }

    /**
     *
     * @return string
     */
    public function getCssClassActive()
    {
        return $this->cssClassActive;
    }

    /**
     *
     * @return string
     */
    public function getCssClassOnPath()
    {
        return $this->cssClassOnPath;
    }

    /**
     *
     * @return Closure
     */
    public function getEscapeFunction()
    {
        return $this->escapeFunction;
    }
}
