function StringUtils() {}

StringUtils.trim = function(str, chars)
{
    return StringUtils.ltrim(StringUtils.rtrim(str, chars), chars);
}

StringUtils.ltrim = function(str, chars)
{
    chars = chars || "\\s";
    return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}

StringUtils.rtrim = function(str, chars)
{
    chars = chars || "\\s";
    return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}
