<?php

namespace mermshaus\NavGenerator;

use Closure;
use Exception;

/**
 *
 */
final class Generator
{
    /**
     *
     * @var Config
     */
    private $config;

    /**
     *
     * @var array
     */
    private $menuStruct = null;

    /**
     *
     * @param Config $config
     */
    public function __construct(Config $config = null)
    {
        $this->config = (null === $config) ? new Config() : $config;
    }

    /**
     *
     * @param array $elements
     * @param Closure $funcHasParent
     * @param Closure $funcIsChild
     * @return array
     */
    private function treeify(array $elements, Closure $funcHasParent, Closure $funcIsChild)
    {
        $root = array(
            'data' => null,
            'children' => array()
        );

        $rec = function ($testElement) use ($elements, &$rec, $funcIsChild) {
            $childElements = array();

            foreach ($elements as $element) {
                if ($funcIsChild($element, $testElement)) {
                    $childElements[] = array(
                        'data' => $element,
                        'children' => $rec($element)
                    );
                }
            }

            return $childElements;
        };

        foreach ($elements as $element) {
            if (false === $funcHasParent($element)) {
                $root['children'][] = array(
                    'data' => $element,
                    'children' => $rec($element)
                );
            }
        }

        return $root;
    }

    /**
     *
     * @param array $tree
     * @param Closure $funcNodeToString
     * @return string
     */
    private function treeToHtmlList(array $tree, Closure $funcNodeToString, Closure $onListStart = null)
    {
        if (count($tree['children']) === 0) {
            return '';
        }

        if (null == $onListStart) {
            $onListStart = function () {
                return '<li>';
            };
        }

        $html = '';

        $rec = function ($node, $indent) use (&$rec, &$html, $funcNodeToString, $onListStart) {
            $indentation  = str_repeat('  ', $indent);
            $indentation2 = str_repeat('  ', $indent + 1);

            $html .= $indentation . $onListStart($node['data']) . $funcNodeToString($node['data']);
            if (count($node['children']) !== 0) {
                $html .= "\n" . $indentation2 . '<ul>' . "\n";
            }
            foreach ($node['children'] as $childNode) {
                $html .= $rec($childNode, $indent + 2);
            }
            if (count($node['children']) !== 0) {
                $html .= $indentation2 . '</ul>' . "\n";
                $html .= $indentation . '</li>' . "\n";
            } else {
                $html .= '</li>' . "\n";
            }
        };

        $indent = 1;

        $html = '<ul>' . "\n";

        foreach ($tree['children'] as $node) {
            $rec($node, $indent);
        }

        $html .= '</ul>' . "\n";

        return $html;
    }

    /**
     *
     * @param type $stream
     * @throws Exception
     */
    public function readFlatFile($stream)
    {
        $menu = array();
        $nextId = 1;
        $stack = array();

        $line = 0;

        while (($buffer = fgets($stream)) !== false) {
            $line++;
            // Skip empty lines
            if (trim($buffer) === '') {
                continue;
            }

            if (0 === strpos(trim($buffer), '#')) {
                continue;
            }

            $parts = array();

            if (0 === preg_match('/^( *)([^;]+);(.+)$/', rtrim($buffer), $parts)) {
                throw new Exception("Malformed line error on line $line");
            }

            if (strlen($parts[1]) % 4 !== 0) {
                throw new Exception(
                    "Indentation error on line $line. Use multiples of four spaces"
                );
            }

            $level = strlen($parts[1]) / 4;
            $title = trim($parts[2]);
            $link = trim($parts[3]);

            if (count($stack) < $level) {
                throw new Exception("Nesting error on line $line: $title");
            }

            while ($level < count($stack)) {
                array_pop($stack);
            }

            $parentId = null;

            if (count($stack) > 0) {
                $parentId = $stack[count($stack) - 1];
            }

            $menu[] = array(
                'id' => $nextId,
                'parent_id' => $parentId,
                'title' => $title,
                'link' => $link
            );

            array_push($stack, $nextId);

            $nextId++;
        }

        $this->menuStruct = $menu;
    }

    /**
     *
     *
     * Here be dragons.
     *
     * @return string
     * @throws Exception
     */
    public function renderMenu()
    {
        $menu = $this->menuStruct;

        if ($menu === null) {
            throw new Exception('No menu struct in memory. Did you load one?');
        }

        $tree = $this->treeify(
            $menu,
            function ($elem) {
                return $elem['parent_id'] !== null;
            },
            function ($elem, $possibleParentElem) {
                return $elem['parent_id'] === $possibleParentElem['id'];
            }
        );

        $isNodeOnActivePath = function ($data, $activePath) use ($menu) {
            $idToMenuElement = array();

            foreach ($menu as &$elem) {
                $idToMenuElement[$elem['id']] = $elem;
            }
            unset($elem);

            $nodeId = $data['id'];

            $activeNode = null;

            // Get active node
            foreach ($menu as $testNode) {
                if ($testNode['link'] === $activePath) {
                    $activeNode = $testNode;
                }
            }

            if ($activeNode['id'] === $nodeId) {
                return true;
            }

            if ($activeNode === null) {
                return false;
            }

            // Get parent ids for active node
            $pids = array();

            $x = $activeNode;

            while ($x['parent_id'] !== null) {
                $x = $idToMenuElement[$x['parent_id']];
                $pids[] = $x['id'];
            }

            if (in_array($nodeId, $pids)) {
                return true;
            }

            return false;
        };

        $isNodeSelected = function ($data, $activePath) {
            if ($data['link'] === $activePath) {
                return true;
            }
            return false;
        };

        $html = $this->treeToHtmlList(
            $tree,
            function ($data) {
                $f = $this->config->getEscapeFunction();

                return '<a href="' . $f($this->config->getPathPrefix() . $data['link']) . '">'
                    . $f($data['title']) . '</a>';
            },
            function ($data) use ($isNodeOnActivePath, $isNodeSelected) {
                $classes = array();

                if ($isNodeOnActivePath($data, $this->config->getActivePath())) {
                    $classes[] = $this->config->getCssClassOnPath();
                }

                if ($isNodeSelected($data, $this->config->getActivePath())) {
                    $classes[] = $this->config->getCssClassActive();
                }

                if (count($classes) > 0) {
                    return '<li class="' . implode(' ', $classes) . '">';
                }

                return '<li>';
            }
        );

        return $html;
    }
}
