<?php

error_reporting(E_ALL | E_STRICT);

function al($name)
{
    include_once './lib/' . str_replace('_', '/', $name) . '.php';
}

spl_autoload_register('al');

header('content-type: text/html; charset=utf-8');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>MenuBuilder</title>
    <link rel="stylesheet" href="res/styles.css" type="text/css" media="screen" />
    <script type="text/javascript" src="js/StringUtils.js"></script>
    <script type="text/javascript" src="js/Storage.js"></script>
    <script type="text/javascript" src="js/MenuBuilder.js"></script>
</head>
<body>
<script type="text/javascript">
var menuBuilder = new MenuBuilder();
var menuOpen = new Array();
var storage = new Storage();

if (storage.get('menuOpen') != null)
{
    menuOpen = storage.get('menuOpen').split(',');
}
</script>
<div class="menu">
<?php

$path = (isset($_GET['path'])) ? $_GET['path'] : '';

$mb = org_ermshaus_menubuilder_Factory::createNewBuilder();
$mb->loadFromFile('./menudata.txt');
echo $mb->createMenuJS($path);

?>
</div>
<div class="content">
<div class="breadcrumbs"><?php echo $mb->createBreadcrumbs($path); ?></div>

<h1><?php echo htmlentities($path); ?></h1>
<p>Validate <a href="http://validator.w3.org/check/referer">XHTML</a>,
<a href="http://jigsaw.w3.org/css-validator/check/referer/">CSS</a></p>
</div>
</body>
</html>
