<?php

#namespace org\ermshaus\menubuilder;

/**
 * MenuBuilder
 *
 * Example file:
 *
 *   ; Menu data
 *   ;
 *   ; Empty lines or lines beginning with a semicolon will be ignored.
 *
 *   Home       | index.php
 *   Foobar     | foobar.php
 *   -Foo       | foo.php
 *   ;-Bar      | bar.php
 *   -Test      | test.php
 *   --Hello    | hello.php
 *
 * @author Marc Ermshaus <http://www.ermshaus.org/>
 * @version 1.0
 */
class org_ermshaus_menubuilder_MainController
{
    /** */
    protected $m_menuRoot;

    /** @todo */
    protected $m_config;

    /** */
    protected $m_runningNumber;

    /** */
    protected $m_jsMenuOpen;

    /** */
    protected $m_index;

    /**
     * Enter description here...
     *
     */
    public function __construct($index)
    {
        $this->m_index = $index;
        $this->m_menuRoot = null;
        $this->m_config = array();
        $this->m_runningNumber = null;
        $this->m_jsMenuOpen = array();
    }

    /**
     * Enter description here...
     *
     * @param string $file
     */
    public function loadFromFile($file)
    {
        $data = file($file);
        $stack = array();
        $root = new org_ermshaus_menubuilder_MenuEntry('root', '');
        $stack[] = $root;

        $curDepth = 0;

        foreach ($data as $e) {
            $e = trim($e);

            if ($e != '' && substr($e, 0, 1) != ';') {
                list($text, $url) = explode('|', $e);
                $text = trim($text);
                $url = trim($url);
                $depth = strlen($text) - strlen(ltrim($text, '-'));
                $text = ltrim($text, '-');

                $me = new org_ermshaus_menubuilder_MenuEntry($text, $url);

                if ($depth > $curDepth) {
                    $parent = $stack[count($stack) - 1];
                } else if ($depth < $curDepth) {
                    array_pop($stack);

                    while ($depth < $curDepth) {
                        array_pop($stack);
                        $curDepth--;
                    }
                    $parent = $stack[count($stack) - 1];
                } else {
                    if (count($stack) > 1) {
                        array_pop($stack);
                    }
                    $parent = $stack[count($stack) - 1];
                }

                array_push($stack, $me);

                $parent->addChild($me);
                $curDepth = $depth;
            }
        }

        $this->m_menuRoot = $root;
    }

    /**
     * Enter description here...
     *
     * @param org_ermshaus_menubuilder_MenuEntry $elem
     * @param int $depth
     */
    public function printDebug(org_ermshaus_menubuilder_MenuEntry $elem = null, $depth = 0)
    {
        if ($elem == null) {
            $elem = $this->m_menuRoot;
        }

        foreach($elem->getChildren() as $child) {
            echo str_repeat('  ', $depth) . $child->getText() . "\n";
            $this->printDebug($child, $depth + 1);
        }
    }

    /**
     * Enter description here...
     *
     * @param org_ermshaus_menubuilder_MenuEntry $tree
     * @param string $activeUrl
     * @return bool
     */
    private function _isUrlInTree(org_ermshaus_menubuilder_MenuEntry $tree, $activeUrl)
    {
        $ret = false;

        $children = $tree->getChildren();
        $i = 0;

        while (!$ret && $i < count($children)) {
            $child = $children[$i];

            if ($child->getUrl() == $activeUrl) {
                $ret = true;
            } else {
                $ret = $this->_isUrlInTree($child, $activeUrl);
            }

            $i++;
        }

        return $ret;
    }

    /**
     * Enter description here...
     *
     * @param string $activeUrl
     * @return string HTML output
     */
    public function createMenu($activeUrl)
    {
        return $this->_createMenuRec($activeUrl);
    }

    public function createMenuJS($activeUrl)
    {
        $this->m_runningNumber = 0;
        $this->m_jsMenuOpen = array();

        $ret = $this->_createMenuJSRec($activeUrl);

        $ret .= '<script type="text/javascript">' . "\n";

        for ($i = 0; $i < count($this->m_jsMenuOpen); $i++) {
            $ret .= "menuBuilder.pushIfNotExists('" . $this->m_jsMenuOpen[$i] . "');\n";
        }

        $ret .= 'menuBuilder.updateOpened();' . "\n";

        $ret .= '</script>' . "\n";

        return $ret;
    }

    /**
     * Enter description here...
     *
     * @param string $activeUrl
     * @param org_ermshaus_menubuilder_MenuEntry $elem
     * @return string HTML output
     */
    private function _createMenuJSRec($activeUrl, org_ermshaus_menubuilder_MenuEntry $elem = null, $depth = 0, $open = true)
    {
        if ($elem == null) {
            $elem = $this->m_menuRoot;
        }

        if (!$open) {
            $out = '<ul id="menu' . $this->m_runningNumber . '" class="hidden">';
        } else {
            $this->m_jsMenuOpen[] = 'menu' . $this->m_runningNumber;
            $out = '<ul id="menu' . $this->m_runningNumber . '">';
        }

        $this->m_runningNumber++;

        foreach ($elem->getChildren() as $child) {
            $classes = 'depth' . $depth . ' ';

            $open = false;

            if ($child->countChildren() > 0) {
                $classes .= 'haschildren ';
            }

            if ($this->_isUrlInTree($child, $activeUrl)
                || ($child->getUrl() == $activeUrl)
            ) {
                $open = true;
                $classes .= 'open ';

                if ($child->getUrl() == $activeUrl) {
                    $classes .= 'active ';
                }
            }

            $urlPart = $this->_makeUrl($child->getUrl());

            $out .= '<li class="clear ' . $classes . '">';
            if ($child->countChildren() > 0) {
                if ($open) {
                    $out .= '<div id="menu' . $this->m_runningNumber . 'h" class="extract extracted" onclick="menuBuilder.toggle(this);">';
                } else {
                    $out .= '<div id="menu' . $this->m_runningNumber . 'h" class="extract collapsed" onclick="menuBuilder.toggle(this);">';
                }
                $out .= '</div>';
            }
            $out .= '<a class="' . $classes . '" href="' . $urlPart . '">'
                    . $child->getText() . '</a>';

            if ($child->countChildren() > 0) {
                $out .= $this->_createMenuJSRec($activeUrl, $child, $depth + 1, $open);
            }

            $out .= '</li>';
        }

        $out .= '</ul>';

        return $out;
    }

    /**
     * Enter description here...
     *
     * @param string $key
     * @param string $value
     * @todo
     */
    public function setConfig($key, $value)
    {
        $this->m_config[$key] = $value;
    }

    /**
     * Enter description here...
     *
     * @param string $activeUrl
     * @param org_ermshaus_menubuilder_MenuEntry $elem
     * @return string HTML output
     */
    private function _createMenuRec($activeUrl, org_ermshaus_menubuilder_MenuEntry $elem = null, $depth = 0)
    {
        if ($elem == null) {
            $elem = $this->m_menuRoot;
        }

        $out = '<ul>';

        foreach ($elem->getChildren() as $child) {
            $open = false;
            $classes = 'depth' . $depth . ' ';

            if ($child->countChildren() > 0) {
                $classes .= 'haschildren ';
            }

            if ($this->_isUrlInTree($child, $activeUrl)
                || ($child->getUrl() == $activeUrl)
            ) {
                $open = true;
                $classes .= 'open ';

                if ($child->getUrl() == $activeUrl) {
                    $classes .= 'active ';
                }
            }

            $urlPart = $this->_makeUrl($child->getUrl());

            $out .= '<li class="clear ' . $classes . '">';
            $out .= '<a class="' . $classes . '" href="' . $urlPart . '">'
                  . $child->getText() . '</a>';

            if ($child->countChildren() > 0 && $open) {
                $out .= $this->_createMenuRec($activeUrl, $child, $depth + 1);
            }

            $out .= '</li>';
        }

        $out .= '</ul>';

        return $out;
    }

    /**
     * Enter description here...
     *
     * @param string $path
     * @return string
     */
    private function _makeUrl($path)
    {
        $ret = '';

        if ($path == '') {
            $ret = './';
        } else {
            $ret = '?path=' . $path;
        }

        return $ret;
    }

    /**
     * Enter description here...
     *
     * @param string $activeUrl
     * @return string HTML output
     */
    public function createBreadcrumbs($activeUrl)
    {
        $out = '';
        $items = $this->_createBreadcrumbsRec($activeUrl);

        for ($i = 0; $i < count($items); $i++) {
            $out .= '<a href="' . $this->_makeUrl($items[$i]->getUrl()) . '">'
                    . $items[$i]->getText() . '</a>';

            if ($i < count($items) - 1) {
                $out .= ' &rsaquo; ';
            }
        }

        return $out;
    }

    /**
     * Enter description here...
     *
     * @param string $activeUrl
     * @param org_ermshaus_menubuilder_MenuEntry $elem
     * @param int $depth
     * @return string
     */
    private function _createBreadcrumbsRec($activeUrl,
        org_ermshaus_menubuilder_MenuEntry $elem = null, $depth = 0)
    {
        if ($elem == null) {
            $elem = $this->m_menuRoot;
        }

        $ret = array();

        foreach ($elem->getChildren() as $child) {
            if ($this->_isUrlInTree($child, $activeUrl)
                || $child->getUrl() == $activeUrl
            ) {
                $ret[] = $child;

                if ($child->getUrl() != $activeUrl) {
                    $ret = array_merge(
                        $ret,
                        $this->_createBreadcrumbsRec(
                            $activeUrl,
                            $child, $depth + 1
                        )
                    );
                }
            }
        }

        return $ret;
    }
}
