<?php

use mermshaus\NavGenerator\Config;
use mermshaus\NavGenerator\Generator;

error_reporting(-1);
ini_set('display_errors', 1);

require_once __DIR__ . '/../../vendor/autoload.php';

$menuData = <<<EOT
# Lines starting with a hash sign (#) will be ignored
#
# Whitespace before/after the separator doesn't matter.
# Indentation with exactly 4 spaces per level is important.
Home                  ; /

About                 ; /about
    The Company       ; /about/company
        Germany       ; /germany
        Europe        ; /europe
        ROW           ; /about/company/row
    Team              ; /about/team
    Careers           ; /about/careers

Services              ; /services
    What we do        ; /services/what-we-do
    Our process       ; /services/our-process
    Testimonials      ; /services/testimonials

Portfolio                    ; /portfolio
    Web Design               ; /portfolio/webdesign
    Development              ; /portfolio/development
    Identity                 ; /portfolio/identity
    SEO & Internet Marketing ; /portfolio/seo-marketing
    Print Design             ; /portfolio/print-design

Contact               ; /contact
EOT;

$navGenerator = new Generator(new Config(rtrim(dirname($_SERVER['SCRIPT_NAME']), '/') . '/?page='));

$url = 'data://text/plain;base64,' . base64_encode($menuData);
$stream = fopen($url, 'rb');
$navGenerator->readFlatFile($stream);
fclose($stream);

$menuHtml = $navGenerator->renderMenu();

header('Content-Type: text/html; charset=UTF-8');

?><!DOCTYPE html>

<html lang="en">

    <head>
        <meta charset="UTF-8" />
        <title>title</title>
        <style>
            a { color: #3399ff; text-decoration: none; }
            .onpath > a { background: rgba(255, 0, 0, 0.1); }
            .selected > a { font-weight: bold; text-decoration: underline; }
        </style>
    </head>

    <body>
        <nav>
        <?=$menuHtml?>
        </nav>
    </body>

</html>
