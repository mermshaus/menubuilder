<?php

class org_ermshaus_menubuilder_Factory
{
    private static $ms_count = 0;

    private function __construct()
    {
        // Can't create instance
    }

    public static function createNewBuilder()
    {
        $mbi = new org_ermshaus_menubuilder_MainController(self::$ms_count);
        self::$ms_count++;
        return $mbi;
    }
}
