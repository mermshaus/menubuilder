<?php

namespace mermshaus\NavGenerator\Test;

use mermshaus\NavGenerator\Config;
use mermshaus\NavGenerator\Generator;
use PHPUnit_Framework_TestCase;

/**
 *
 */
class GeneralTest extends PHPUnit_Framework_TestCase
{
    public function testCanBeInstantiated()
    {
        new Generator(new Config());
    }
}
