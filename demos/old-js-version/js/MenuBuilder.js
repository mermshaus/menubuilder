/**
 *
 */
function MenuBuilder() {}

/**
 *
 */
MenuBuilder.prototype._hasClass = function(obj, cl)
{
    var ret = false;
    var classes = StringUtils.trim(obj.className).split(' ');
    for (var i = 0; i < classes.length; i++)
    {
        if (cl == classes[i])
        {
            ret = true;
        }
    }

    return ret;
}

/**
 *
 */
MenuBuilder.prototype._addClass = function(obj, cl)
{
    if (!this._hasClass(obj, cl))
    {
        obj.className += ' ' + cl;
    }
}

/**
 *
 */
MenuBuilder.prototype._removeClass = function(obj, cl)
{
    var classes = StringUtils.trim(obj.className).split(' ');
    var newClasses = '';
    for (j = 0; j < classes.length; j++)
    {
        if (cl != classes[j])
        {
            newClasses += classes[j] + ' ';
        }
    }
    obj.className = newClasses;
}

MenuBuilder.prototype._showTree = function(obj)
{
    this._removeClass(obj, 'hidden');
    this.pushIfNotExists(obj.id);
    var elem = document.getElementById(obj.id + 'h');
    this._removeClass(elem, 'collapsed');
    this._addClass(elem, 'extracted');
}

MenuBuilder.prototype._hideTree = function(obj)
{
    var tmp = new Array();

    for (var j = 0; j < menuOpen.length; j++)
    {
        if (menuOpen[j] != obj.id)
        {
            tmp.push(menuOpen[j]);
        }
    }

    menuOpen = tmp;

    this._addClass(obj, 'hidden');
    var elem = document.getElementById(obj.id + 'h');
    this._removeClass(elem, 'extracted');
    this._addClass(elem, 'collapsed');
}

/*
 *
 */
MenuBuilder.prototype.toggle = function(elem)
{
    var i = 0,

    // Oh, Internet Explorer, how I love thee.
    temp = elem.parentElement ? elem.parentElement : elem.parentNode;

    while (i < temp.childNodes.length)
    {
        child = temp.childNodes[i];

        if (child.nodeName != 'A' && child != elem)
        {
            if (this._hasClass(child, 'hidden'))
            {
                this._showTree(child);
            }
            else
            {
                this._hideTree(child);
            }

            storage.set('menuOpen', menuOpen);
        }

        i++;
    }
}

MenuBuilder.prototype.pushIfNotExists = function(s)
{
    var found = false;

    for (var i = 0; i < menuOpen.length; i++)
    {
        if (menuOpen[i] == s)
        {
            found = true;
        }
    }

    if (!found)
    {
        menuOpen.push(s);
        storage.set('menuOpen', menuOpen);
    }
}

MenuBuilder.prototype.updateOpened = function()
{
    for (var i = 0; i < menuOpen.length; i++)
    {
        if (menuOpen[i] != 'menu0')
        {
            this._showTree(document.getElementById(menuOpen[i]));
        }
    }
}
